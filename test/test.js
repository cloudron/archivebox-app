#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

let execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const TEST_TIMEOUT = 20000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const LOCATION = process.env.LOCATION || 'test';

    let USERNAME = process.env.USERNAME;
    let PASSWORD = process.env.PASSWORD;
    let adminUsername = 'admin';
    let adminPassword = 'changeme123';

    let URL = 'https://blog.cloudron.io';

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function sleep(millis) {
        return new Promise(resolve => setTimeout(resolve, millis));
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        let inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function registerLDAPUser(username, password) {
        await browser.get(`https://${app.fqdn}/admin/login/`);
        await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(username);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//input[@value="Log in"]')).click();
        await browser.sleep(2000);
    }

    async function login(username, password) {
/*
        await browser.get(`https://${app.fqdn}/admin/login/`);
        await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(username);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//input[contains(text(), "Log in")]')).click();
        await browser.sleep(2000);
*/
        await registerLDAPUser(username, password);
//        await waitForElement(By.xpath('//a[contains(., "Authentication and Authorization")]'));
        await waitForElement(By.xpath('//a[contains(., "Log out")]'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath('//a[contains(., "Log out")]'));
        await browser.findElement(By.xpath('//a[contains(., "Log out")]')).click();
        await waitForElement(By.xpath('//th[text()="Bookmarked"]'));
    }


    async function createSnapshot(url) {
        await browser.get(`https://${app.fqdn}/add`);
        await waitForElement(By.xpath('//h1[contains(text(), "Add new URLs to your archive")]'));
        await sleep(2000);
        await browser.findElement(By.xpath('//textarea[@name="url"]')).sendKeys(url);
        await sleep(2000);
        await browser.findElement(By.xpath('//button[contains(text(), "Add URLs and archive")]')).click();
        // wait for a while to get snapshot finished
        console.log("Please wait for 40 seconds...");
        await sleep(40000);
        await checkSnapshot(url);
    }

    async function checkSnapshot(url) {
        await browser.get(`https://${app.fqdn}/public`);
        await sleep(2000);
        await waitForElement(By.xpath(`//a[@href="${url}" and contains(., "${url}")]`));
    }
 
    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    // No SSO
    it('install app (No SSO)', function () { execSync(`cloudron install --no-sso --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, adminUsername, adminPassword));

    it('can create Snapshot', createSnapshot.bind(null, URL));
    it('can check Snapshot', checkSnapshot.bind(null, URL));

    it('can admin logout', logout);

    it('uninstall app', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
            done();
        });
    });

    // SSO
    it('install app (SSO)', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can register LDAP user', registerLDAPUser.bind(null, USERNAME, PASSWORD));
    it('can update LDAP user as admin', async function() {
        execSync(`cloudron exec --app ${app.id} -- bash -c "cd /app/data/archivebox && gosu cloudron:cloudron archivebox manage shell <<EOF
from django.contrib.auth.models import User
User.objects.filter(username='${USERNAME}').update(is_superuser=True, is_staff=True)
EOF"`);
    });

    it('can login', login.bind(null, USERNAME, PASSWORD));

    it('can create Snapshot', createSnapshot.bind(null, URL));
    it('can check Snapshot', checkSnapshot.bind(null, URL));

    it('can logout', logout);

    it('can restart app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron restart --app ${app.id}`);
    });

    it('can login', login.bind(null, USERNAME, PASSWORD));
    it('can check Snapshot', checkSnapshot.bind(null, URL));
    it('can logout', logout);

    it('backup app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron backup create --app ${app.id}`);
    });
    it('restore app', function (done) {
        browser.get('about:blank').then(function () {
            const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
            execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
            execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
            getAppInfo();
            execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
            done();
        });
    });

    it('can login', login.bind(null, USERNAME, PASSWORD));
    it('can check Snapshot', checkSnapshot.bind(null, URL));
    it('can logout', logout);

    it('move to different location', function (done) {
        browser.manage().deleteAllCookies();
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron configure --app ${app.id} --location ${LOCATION}2`, EXEC_ARGS);
            done();
        });
    });
    it('can get app information', getAppInfo);

    it('can login', login.bind(null, USERNAME, PASSWORD));
    it('can check Snapshot', checkSnapshot.bind(null, URL));
    it('can logout', logout);

    it('uninstall app', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
            done();
        });
    });

    // test update
/*
    it('can install app', function () { execSync(`cloudron install --appstore-id io.archivebox.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can register LDAP user', registerLDAPUser.bind(null, USERNAME, PASSWORD));
    it('can login', login.bind(null, USERNAME, PASSWORD));

    it('can create Snapshot', createSnapshot.bind(null, URL));
    it('can check Snapshot', checkSnapshot.bind(null, URL));

    it('can logout', logout);

    it('can update', function () {
        browser.get('about:blank').then(function () {
            execSync(`cloudron update --app ${LOCATION}`, EXEC_ARGS);
        });
    });

    it('can get app information', getAppInfo);

    it('can login', login.bind(null, USERNAME, PASSWORD));

    it('can check Snapshot', checkSnapshot.bind(null, URL));

    it('can logout', logout);

    it('uninstall app', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
            done();
        });
    });
*/
});
