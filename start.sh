#!/bin/bash
set -eu

mkdir -p /home/cloudron/.config/chromium/Crash\ Reports/pending \
        /run/cloudron.local \
        /run/cloudron.pki

export CHROME_BINARY="$(gosu cloudron:cloudron python3 -c 'from playwright.sync_api import sync_playwright; print(sync_playwright().start().chromium.executable_path)')"

export USE_SINGLEFILE=true
export SINGLEFILE_BINARY="/app/code/bin/single-file"
export USE_READABILITY=true
export READABILITY_BINARY="/app/code/bin/readability-extractor"
export USE_MERCURY=true
export MERCURY_BINARY="/app/code/bin/postlight-parser"

echo "==> Updating permissions"
chown -R cloudron:cloudron /run/cloudron.local /run/cloudron.pki /app/data /home/cloudron/.config/chromium

if [[ ! -f /app/data/archivebox/ArchiveBox.conf ]]; then

    echo "==> Init ArchiveBox"

    gosu cloudron:cloudron mkdir -p /app/data/archivebox

    cd /app/data/archivebox
    gosu cloudron:cloudron archivebox init

    # web server settings (can be changed in /app/data/archivebox/ArchiveBox.conf)
    gosu cloudron:cloudron archivebox config --set \
        PUBLIC_INDEX=True \
        PUBLIC_SNAPSHOTS=False \
        PUBLIC_ADD_VIEW=False \
        DEBUG=False \
        CHROME_BINARY=${CHROME_BINARY}

    if [[ -z "${CLOUDRON_LDAP_SERVER:-}" ]]; then
        echo "=> Creating initial super user (admin / changeme123)"
        export DJANGO_SUPERUSER_PASSWORD="changeme123"
        gosu cloudron:cloudron archivebox manage createsuperuser --username admin --email admin@cloudron.local --no-input
    else
        gosu cloudron:cloudron archivebox config --set \
            LDAP_CREATE_SUPERUSER=True
    fi
fi

cd /app/data/archivebox
if [[ -n "${CLOUDRON_LDAP_SERVER:-}" ]]; then
    echo "==> Updating LDAP config"

    gosu cloudron:cloudron archivebox config --set \
        LDAP_ENABLED=True \
        LDAP_SERVER_URI="${CLOUDRON_LDAP_URL}" \
        LDAP_BIND_DN="${CLOUDRON_LDAP_BIND_DN}" \
        LDAP_BIND_PASSWORD="${CLOUDRON_LDAP_BIND_PASSWORD}" \
        LDAP_USER_BASE="${CLOUDRON_LDAP_USERS_BASE_DN}" \
        LDAP_USER_FILTER="(|(objectclass=user))" \
        LDAP_USERNAME_ATTR="username" \
        LDAP_FIRSTNAME_ATTR="givenName" \
        LDAP_LASTNAME_ATTR="sn" \
        LDAP_EMAIL_ATTR="mail"

fi

gosu cloudron:cloudron archivebox config --set \
    CHROME_SANDBOX=false \
    CHROME_HEADLESS=true \
    ALLOWED_HOSTS=* \
    CSRF_TRUSTED_ORIGINS=${CLOUDRON_APP_ORIGIN}

echo "==> Starting ArchiveBox web server"
exec gosu cloudron:cloudron archivebox server 0.0.0.0:8000
