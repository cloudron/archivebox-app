<sso>
By default, Cloudron users become admins on the first login.

If you'd like new users to have regular user permissions, 
you should change `LDAP_CREATE_SUPERUSER` value to False in
 /app/data/archivebox/ArchiveBox.conf, e.g.
```
LDAP_CREATE_SUPERUSER = False
```
Restart the app for the changes to become active.

</sso>

<nosso>
This app is pre-setup with an admin account. The initial credentials are:

**Username**: admin<br/>
**Password**: changeme123<br/>

Please change the admin password immediately!
</nosso>
