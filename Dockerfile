FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

ARG DEBIAN_FRONTEND=noninteractive

RUN mkdir -p /app/code /app/pkg /home/cloudron/.pki

WORKDIR /app/pkg

ARG ARCHIVEBOX_VERSION=0.8.5rc51

RUN apt-get update -qq && \
    apt-get install -y --no-install-recommends \
        python3-distutils \
        python3-ldap \
        python3-msgpack \
        python3-mutagen \
        python3-regex \
        python3-pycryptodome \
        youtube-dl \
        ripgrep \
        procps && \
        rm -rf /var/lib/apt/lists/*

ENV PYTHONPATH=/app/code/packages
ENV PATH=$PATH:$PYTHONPATH/bin:/app/code/bin

# this will install the coredeps and the additional ldap
RUN mkdir -p $PYTHONPATH && \
    pip install --upgrade --target $PYTHONPATH \
        "archivebox[ldap,debug]==${ARCHIVEBOX_VERSION}" \
        playwright

# install Chromium using playwright
ENV PLAYWRIGHT_BROWSERS_PATH=/app/code/browsers
RUN mkdir -p $PLAYWRIGHT_BROWSERS_PATH && \
    playwright install --with-deps chromium

# install ArchiveBox dependencies
# https://github.com/ArchiveBox/ArchiveBox/blob/main/package.json
RUN npm install -g --prefix /app/code \
                "git+https://github.com/pirate/readability-extractor" \
                "@postlight/parser@^2.2.3" \
                "single-file-cli@^1.1.46"

RUN ln -sf /run/cloudron.local /home/cloudron/.local && \
    ln -sf /run/cloudron.pki /home/cloudron/.pki

# https://github.com/ArchiveBox/ArchiveBox/blob/v0.8.5rc51/archivebox/plugins_auth/ldap/config.py#L71
RUN sed -i '/.*update_in_place(LDAP_ENABLED=False)/d' /app/code/packages/archivebox/plugins_auth/ldap/config.py

RUN chown -R cloudron:cloudron /app/code /home/cloudron

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
